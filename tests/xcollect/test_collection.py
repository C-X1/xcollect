"""Test Collector Class."""

import docker
import pytest
from mock import Mock

from xcollect.collection import executables_from_directory, path_from_env, var_from_env


class TestCollection(object):
    """Test collection functions."""

    def setup(self):
        """Execute before each test."""

    @staticmethod
    def test_var_from_env():
        """Test extracting env value."""
        env = ["DISPLAY=:0", "PATH=/opt/:/usr/bin:/usr/sbin"]

        result = [
            var_from_env(env, "DISPLAY"),
            var_from_env(env, "PATH"),
            var_from_env(env, "NOTTHERE"),
        ]

        assert result == [":0", "/opt/:/usr/bin:/usr/sbin", None]

    @staticmethod
    def test_path_from_env():
        """Test getting the entries from a path string."""
        env_present_path = ["DISPLAY=:0", "PATH=/opt/:/usr/bin:/usr/sbin"]
        env_missing_path = ["DISPLAY=:0"]

        result = [path_from_env(env_present_path), path_from_env(env_missing_path)]

        assert result == [["/opt/", "/usr/bin", "/usr/sbin"], []]

    @staticmethod
    def test_executables_from_directory_exec_good():
        """Make dry test of string handling."""
        container = Mock(spec=docker.models.containers.Container)
        container.exec_run = Mock(
            return_value=(Mock(exit_code=0, output=b"cat\necho\nls"))
        )

        result = executables_from_directory(container, "/usr/bin")

        assert result == ["cat", "echo", "ls"]

    @staticmethod
    def test_executables_from_directory_exec_empty():
        """Make dry test of string handling."""
        container = Mock(spec=docker.models.containers.Container)
        container.exec_run = Mock(return_value=(Mock(exit_code=0, output=b"\n")))

        result = executables_from_directory(container, "/usr/bin")

        assert result == []

    @staticmethod
    def test_executables_from_directory_exec_error():
        """Make dry test of string handling."""
        container = Mock(spec=docker.models.containers.Container)
        container.exec_run = Mock(
            return_value=(
                Mock(exit_code=1, output=b"find: '/notthere': File not found")
            )
        )

        with pytest.raises(FileNotFoundError):
            executables_from_directory(container, "/notthere")
