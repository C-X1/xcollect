"""Implement FolderedTest."""


class FolderedTest(object):
    """Create a test folder for each test and change cwd to it.

    This is usefull for code interacting with files in the
    current working directory.
    """

    def setup(self):
        """Execute before each test."""

    def test_folder(self):
        """Create the folder for the current test."""

    def resource(self, resource_name):
        """Copy a resource from the resources to the current test folder.

        :param resource_name: Path of the file/folder inside resources
        """
