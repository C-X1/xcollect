"""Define command for kubernetes."""

from xcollect.cli import app


@app.command()
def kube():
    """Run collector for kubernetes."""
