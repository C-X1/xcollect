"""Define command for docker."""


from typing import List

from xcollect.cli import app
from xcollect.cli.args import Arguments, Options


@app.command()
def docker(
    containers: List[str] = Arguments.containers_argument,
    socket_url: str = Options.socket_url,
):
    """Run collector for docker.

    :param containers: Containers to collect executables from
    :param socket_url: Base url for Container Engine
    """
