"""Define command for compose."""

from xcollect.cli import app


@app.command()
def compose():
    """Run collector for compose."""
