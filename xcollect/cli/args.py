"""Define application arguments."""

import typer

from xcollect.collection import container_list
from xcollect.const import DOCKER_DEFAULT_BASE_URL


class Completions(object):
    """Implement completion methods."""

    @staticmethod
    def complete_docker_container(ctx: typer.Context, incomplete: str):
        """Complete docker container list.

        :param ctx: Typer Context
        :param incomplete: Current string entered for completion
        :yield: Completed Container name
        """
        socket_url = ctx.params.get("socket_url")
        containers = ctx.params.get("containers") or []
        containers_on_engine = container_list(socket_url)

        for container in containers_on_engine:
            if container.startswith(incomplete) and container not in containers:
                yield container


class Arguments(object):
    """Implement Arguments for typer."""

    containers_argument = typer.Argument(
        ...,
        help="The docker containers to scan",
        autocompletion=Completions.complete_docker_container,
    )


class Options(object):
    """Implement Options for typer."""

    socket_url = typer.Option(
        DOCKER_DEFAULT_BASE_URL, help="The URL to the engine socket"
    )
