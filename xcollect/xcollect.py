"""Construct and run main application xcollect."""

import typer

from xcollect.cli import app

# Typer imports "used" by @app.command() decorator in the file
from xcollect.cli.compose import compose  # noqa: F401
from xcollect.cli.docker import docker  # noqa: F401
from xcollect.cli.kube import kube  # noqa: F401


@app.callback(no_args_is_help=True)
def callback():
    """Show help when called with no arguments."""


def main():
    """Construct typer_click interface for xcollect."""
    typer_click = typer.main.get_command(app)
    typer_click()


if __name__ == "__main__":
    main()
