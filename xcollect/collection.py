"""Collect of executables."""


import docker
from structlog import get_logger

from xcollect.const import DOCKER_DEFAULT_BASE_URL


def var_from_env(env: list, name: str) -> str:
    """Get variable from env.

    :param env: The list of environment entries
    :param name: The name of the variable to extract
    :return: value of environment variable
    :rtype: str
    """
    var_value = None
    for entry in env:
        identifier = "{name}=".format(name=name)
        if entry.startswith(identifier):
            var_value = entry[len(identifier) :]
            break

    return var_value


def path_from_env(env: list) -> list:
    """Extract the PATH directory list from env.

    :param env: The list of environment entries
    :return: List of paths in PATH
    :rtype: list
    """
    paths = []
    path = var_from_env(env, "PATH")

    if path is not None:
        paths = path.split(":")
    return paths


def executables_from_directory(container: str, directory: str):
    """Collect executables from container in specific directory.

    :param container: Container object to retrieve the executables from
    :param directory: Path of directory
    :return: List of executable files in directory
    :raises FileNotFoundError: if path is invalid
    """
    exec_result = container.exec_run(
        "find {directory} -maxdepth 1 -executable".format(directory=directory)
    )

    output = exec_result.output.decode("utf-8")

    if exec_result.exit_code != 0:
        raise FileNotFoundError(output)

    directories = output.strip().split("\n")

    if "" in directories:
        directories.remove("")

    return directories


def executables_from_container(container_name, base_url=DOCKER_DEFAULT_BASE_URL):
    """Collect executables from container.

    :param container_name: Name or ID of the container
    :param base_url:  URL of docker engine
    :return: List of executable paths of supplied container
    """
    container = docker.DockerClient(base_url).containers.get(container_name)
    env = container.attrs["Config"]["Env"]
    executables = []
    log = get_logger()
    for directory in path_from_env(env):
        try:
            executables += executables_from_directory(container, directory)
        except FileNotFoundError as file_not_found_error:
            log.exception(file_not_found_error)
    return executables


def container_list(base_url=DOCKER_DEFAULT_BASE_URL):
    """Collect container names from engine.

    :param base_url:  URL of docker engine
    :return: List of docker containers
    """
    return [
        container.name for container in docker.DockerClient(base_url).containers.list()
    ]
