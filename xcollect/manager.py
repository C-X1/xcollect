"""Implement manager."""


class Manager(object):
    """Manage command supply."""

    def __init__(self, mode: str):
        """Initialize class.

        :param mode: The mode valid modes are docker, compose, kube
        """
        self.mode = mode

    def serve(self):
        """Run and update constantly."""

    def create(self):
        """Create links depending on the mode."""
